#ifndef SOCK_FOR_FD_H
#define SOCK_FOR_FD_H
#include <ruby.h>

static ID id_for_fd;
static VALUE sock_for_fd(VALUE klass, int fd)
{
	return rb_funcall(klass, id_for_fd, 1, INT2NUM(fd));
}

static void init_sock_for_fd(void)
{
	id_for_fd = rb_intern("for_fd");
}
#endif /* SOCK_FOR_FD_H */
