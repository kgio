#include <ruby.h>
#include <ruby/io.h>

static int my_fileno(VALUE io)
{
#ifdef HAVE_RB_IO_DESCRIPTOR
	if (TYPE(io) != T_FILE)
		io = rb_io_get_io(io);

	return rb_io_descriptor(io);
#else
	rb_io_t *fptr;
	int fd;

	if (TYPE(io) != T_FILE)
		io = rb_io_get_io(io);
	GetOpenFile(io, fptr);
	rb_io_check_closed(fptr);
	return fptr->fd;
#endif
}
