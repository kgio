/* We do not modify RSTRING in this file, so RSTRING_MODIFIED is not needed */
static void set_file_path(VALUE io, VALUE path)
{
	rb_iv_set(io, "@path", rb_str_new4(path));
}
