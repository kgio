ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }

Gem::Specification.new do |s|
  s.name = %q{kgio}
  s.version = (ENV["VERSION"] || '2.11.4').dup
  s.homepage = 'https://yhbt.net/kgio/'
  s.authors = ['kgio hackers']
  summary, desc = *(File.read('README').split("\n\n"))
  s.description = desc.strip
  s.email = %q{kgio-public@yhbt.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.summary = summary.sub(/\A[^-]+ - /, '').strip
  s.test_files = Dir['test/test_*.rb']
  s.extensions = %w(ext/kgio/extconf.rb)

  s.add_development_dependency('test-unit', '~> 3.0')
  # s.add_development_dependency('strace_me', '~> 1.0') # Linux only

  s.licenses = %w(LGPL-2.1+)
  s.required_ruby_version = '>= 1.9.3' # kgio is deprecated anyways...
end
